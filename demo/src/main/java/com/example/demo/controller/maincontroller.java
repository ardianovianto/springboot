package com.example.demo.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Employees;
import com.example.demo.repository.employeeRepo;

@RestController
public class maincontroller {

	@Autowired
	private employeeRepo er;
	
	@GetMapping("/getempid")
	public Employees getByEmpId(@RequestParam Integer empId) {
		Employees employees = new Employees();
		
		employees = er.findByEmpID(empId);
		
		return employees;
		
	}
	
	@GetMapping("/getempall")
	public List<Employees> getEmpAll(){
		List<Employees> empls = new ArrayList<Employees>();
		empls = (List<Employees>) er.findAll();
		return empls;
	}
	
	@GetMapping("/getposid")
		public List<Employees> getByPosId(@RequestParam Integer posId){
			List<Employees> empls = new ArrayList<Employees>();
			empls = (List<Employees>) er.findByPosID(posId);
			return empls;
		}
	
	@PostMapping("/setposid")
	public Map<String, String> setEmp (@RequestBody Employees e){
		Map<String, String> respMap = new LinkedHashMap<String, String>();
		try {
			e = er.save(e);
			respMap.put("empid", e.getEmpID().toString());
			respMap.put("code", HttpStatus.OK+"");
			respMap.put("msg", "sucess");
		} catch (Exception e2) {
			respMap.put("empid", "");
			respMap.put("code", HttpStatus.CONFLICT+"");
			respMap.put("msg", "error");
			
		}
		return respMap;
	}
	
}
