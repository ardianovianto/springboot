package com.example.demo.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.model.Employees;

public interface employeeRepo extends CrudRepository<Employees, Integer>{

	public Employees findByEmpID (Integer empId);
	
	public List<Employees> findByPosID (Integer posId);
	
}



